###

Simple Usage breadcrumb.

![alt text](screenshots/01.png)
### Usage
```html
<breadcrumb [items]="['خانه','فروشگاه','لباس مردانه']"></breadcrumb>
```
